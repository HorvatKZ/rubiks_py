from cube import Cube


def main():
    cube = Cube()
    scrambler = Cube.gen_scramble_code()
    print(scrambler)
    print()
    cube.turn(scrambler)
    print(cube)


if __name__ == '__main__':
    main()
