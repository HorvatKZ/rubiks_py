import random


class Cube:

    def __init__(self):
        self.cube = [
            # 0 1 2
            # 3 4 5
            # 6 7 8
            ['W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W'], # White (front)
            ['B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B'], # Blue (top)
            ['O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O'], # Orange (left)
            ['G', 'G', 'G', 'G', 'G', 'G', 'G', 'G', 'G'], # Green (bottom)
            ['R', 'R', 'R', 'R', 'R', 'R', 'R', 'R', 'R'], # Red (right)
            ['Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y'], # Yellow (back)
        ]
        self.front = self.cube[0]
        self.top = self.cube[1]
        self.left = self.cube[2]
        self.bottom = self.cube[3]
        self.right = self.cube[4]
        self.back = self.cube[5]
    

    def turn(self, code: str):
        for c in code.split(' '):
            modifier = c[1] if len(c) > 1 else ''
            match modifier:
                case '':
                    self.__do_one_turn(c[0])
                case '2':
                    self.__do_one_turn(c[0])
                    self.__do_one_turn(c[0])
                case '\'':
                    self.__do_one_turn(c[0])
                    self.__do_one_turn(c[0])
                    self.__do_one_turn(c[0])
    
    
    def scramble(self, length: int = 25):
        self.turn(Cube.gen_scramble_code(length))
    

    @staticmethod
    def gen_scramble_code(length: int = 25):
        TURNS = ['U', 'L', 'D', 'R', 'F', 'B']
        MODIFIERS = ['', '2', '\'']
        return ' '.join([random.choice(TURNS) + random.choice(MODIFIERS) for _ in range(length)])


    def __do_one_turn(self, code: str):
        match code:
            case 'U':
                self.__do_U_turn()
            case 'L':
                self.__do_L_turn()
            case 'D':
                self.__do_D_turn()
            case 'R':
                self.__do_R_turn()
            case 'F':
                self.__do_F_turn()
            case 'B':
                self.__do_B_turn()
    

    def __do_U_turn(self):
        Cube.__turn_lines([self.left, self.front, self.right, self.back], [[0, 1, 2], [0, 1, 2], [0, 1, 2], [0, 1, 2]])
        Cube.__turn_side_clockwise(self.top)


    def __do_L_turn(self):
        Cube.__turn_lines([self.bottom, self.front, self.top, self.back], [[0, 3, 6], [0, 3, 6], [0, 3, 6], [8, 5, 2]])
        Cube.__turn_side_clockwise(self.left)
    

    def __do_D_turn(self):
        Cube.__turn_lines([self.right, self.front, self.left, self.back], [[6, 7, 8], [6, 7, 8], [6, 7, 8], [6, 7, 8]])
        Cube.__turn_side_clockwise(self.bottom)


    def __do_R_turn(self):
        Cube.__turn_lines([self.top, self.front, self.bottom, self.back], [[2, 5, 8], [2, 5, 8], [2, 5, 8], [6, 3, 0]])
        Cube.__turn_side_clockwise(self.right)


    def __do_F_turn(self):
        Cube.__turn_lines([self.top, self.left, self.bottom, self.right], [[6, 7, 8], [8, 5, 2], [2, 1, 0], [0, 3, 6]])
        Cube.__turn_side_clockwise(self.front)


    def __do_B_turn(self):
        Cube.__turn_lines([self.top, self.right, self.bottom, self.left], [[0, 1, 2], [2, 5, 8], [8, 7, 6], [6, 3, 0]])
        Cube.__turn_side_clockwise(self.back)


    @staticmethod
    def __turn_lines(sides: list[list[str]], cells: list[list[int]]):
        temp = [ sides[0][cells[0][0]], sides[0][cells[0][1]], sides[0][cells[0][2]] ]
        sides[0][cells[0][0]], sides[0][cells[0][1]], sides[0][cells[0][2]] = sides[1][cells[1][0]], sides[1][cells[1][1]], sides[1][cells[1][2]]
        sides[1][cells[1][0]], sides[1][cells[1][1]], sides[1][cells[1][2]] = sides[2][cells[2][0]], sides[2][cells[2][1]], sides[2][cells[2][2]]
        sides[2][cells[2][0]], sides[2][cells[2][1]], sides[2][cells[2][2]] = sides[3][cells[3][0]], sides[3][cells[3][1]], sides[3][cells[3][2]]
        sides[3][cells[3][0]], sides[3][cells[3][1]], sides[3][cells[3][2]] = temp[0], temp[1], temp[2]


    @staticmethod
    def __turn_side_clockwise(side: list[str]):
        side[0], side[1], side[2], side[5], side[8], side[7], side[6], side[3] = side[6], side[3], side[0], side[1], side[2], side[5], side[8], side[7]

    
    def __str__(self) -> str:
        result = ''

        # Top
        for i in range(3):
            result += '    '
            for j in range(3):
                result += self.top[3 * i + j]
            result += '\n'
        result += '\n'
        
        # Left/Front/Right/Back
        for i in range(3):
            for j in range(3):
                result += self.left[3 * i + j]
            result += ' '
            for j in range(3):
                result += self.front[3 * i + j]
            result += ' '
            for j in range(3):
                result += self.right[3 * i + j]
            result += ' '
            for j in range(3):
                result += self.back[3 * i + j]
            result += '\n'
        result += '\n'
        
        # Bottom
        for i in range(3):
            result += '    '
            for j in range(3):
                result += self.bottom[3 * i + j]
            result += '\n'
        
        return result
